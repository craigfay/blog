/**
 * Dependencies
 */
const { Entity, constrain } = require('@warpstone/alpha').entity;

const constraints = {
  blocks: [
    constrain.required(),
    constrain.stringType(),
  ],
  meta: [
    constrain.objectType(),
  ],
};

module.exports = class Post extends Entity {
  constructor(fields) {
    super(fields, constraints);
  }
};
