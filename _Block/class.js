/**
 * Dependencies
 */
const { Entity, constrain } = require('@warpstone/alpha').entity;

const constraints = {
  type: [
    constrain.stringType(),
    constrain.required(),
  ],
  args: [
    constrain.objectType(),
  ],
};

module.exports = class Block extends Entity {
  constructor(fields) {
    super(fields, constraints);
  }
};
