/**
 * Application Entry Point
 */

/**
 * Dependencies
 */
const config = require('./config');
const urls = require('./boundaries/web/urls');
const persistence = require('./boundaries/persistence');
const { server } = require('@warpstone/alpha');

/**
 * Open External boundaries
 */
server.start({ ...config, urls });
// persistence.start();