/**
 * Dependencies
 */

/**
 * Render an html string based on a context object
 * @param {Object} context Values to be inserted into the html dynamically
 * @return {String}
 */
const render = context => `
  <div class="column">
    ${Object.keys(context.areas).map(area => `
      <div class="area-title">
        ${area}
        <hr />
      </div>
      ${Object.values(context.areas[area]).map((article) => `
      <a href="${article.url}" class="panel"> 
          <div class="book-title">${article.bookTitle}</div>
          <div class="book-author">${article.bookAuthor}</div>
          <div class="synopsis">${article.synopsis}</div>
        </a>`
      ).join('')}
    `).join('')}
  </div>
`;

module.exports = render;
