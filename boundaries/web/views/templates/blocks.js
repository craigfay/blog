const blocks = {};

blocks.render = function render(block) {
  try {
    return this[block.type](block);
  } catch (e) {
    return '';
  }
}

blocks.paragraph = function paragraph(context) {
  return `<div class="paragraph-body">${context.text}</div>`;
}

blocks.header = function header(context) {
  return `<div class="paragraph-snippet">${context.text}</div>`
}

blocks.image = function image(context) {
  return `<img src=${context.src}></img>`;
}

module.exports = blocks;