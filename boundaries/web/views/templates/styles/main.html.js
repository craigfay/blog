/**
 * Dependencies
 */

/**
 * Render an html string based on a context object
 * @param {Object} context Values to be inserted into the html dynamically
 * @return {String}
 */
const render = context => `
<style>
html, body {
  background-color: #f4f7ff;
  margin: 0px;
  color: #555;
  font-family: Oxygen;
  font-size: 14px;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
}

hr {
  margin: 0px 0px 10px 0px;
  border: 0;
  border-top: 2px solid #ddd; 
}

.navigation {
  color: white;
  background-color: #292935;
  display: flex;
  padding: 20px;
  box-shadow: 0px 0px 2px 3px rgba(0,0,0,.1);
}

.site-title {
  font-size: 24px;
  font-family: Quicksand;
  margin-bottom: 5px;
}

.site-subtitle {
  font-size: 12px;
  text-transform: uppercase;
  color: #aaa;
  font-family: Quicksand;
}

.column {
  box-sizing: border-box;
  flex-direction: column;
  width: 100%;
  min-width: 200px;
  padding-top: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 20px;
}

.panel {
  cursor: pointer;
  box-sizing: border-box;
  width: 100%;
  padding: 20px;
  transition: box-shadow 2s, background-color 1s;
}

.panel:hover {
    background-color: #fff;
    box-shadow: 0px 0px 16px 2px rgba(0,0,0,.15);
}

.book-title, .page-title {
  text-transform: uppercase;
  margin: 0px 0px 4px 0px;
  font-family: Quicksand;
  font-weight: bold;
  font-size: 18px;
}

.book-author {
  margin-bottom: 12px;
  color: #999;
  font-size: 12px;
}

@media only screen and (min-width: 600px) {
  .panel {
    max-width: 600px;
  }
}

/* Individual Page */

.column-no-top {
  box-sizing: border-box;
  flex-direction: column;
  width: 100%;
  min-width: 200px;
  padding-top: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0px 20px;
}

.panel-solid {
  background-color: #fff;
  box-shadow: 0px 0px 16px 2px rgba(0,0,0,.15);
  box-sizing: border-box;
  width: 100%;
  padding: 20px;
}

.panel-soft {
  box-sizing: border-box;
  width: 100%;
  padding: 20px;
}

.page-title {
  font-size: 24px;
}

.article-body p {
  font-size: 14px;
  margin: 0px 0px 20px 0px; 
  color: #333;
  line-height: 1.5;
}

.article .book-author {
  margin: 0;
}

.article .column-no-top {
  padding: 0px 20px;
}

@media only screen and (min-width: 600px) {
  .panel-solid {
    max-width: 600px;
  }
}
</style>
`;

module.exports = render;
