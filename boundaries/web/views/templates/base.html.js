/**
 * Dependencies
 */
const renderHeader = require('./header.html.js');

/**
 * Render an html string based on a context object
 * @param {Object} context Values to be inserted into the html dynamically
 * @return {String}
 */
const render = context => `
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>dotstar</title>

    <!-- Stylesheets -->
    <link href="/styles/main.css" rel="stylesheet">

    <!-- Google Fonts --> 
    <link href="https://fonts.googleapis.com/css?family=Quicksand|Libre+Baskerville" rel="stylesheet">
    
  </head>
  <body>
    <div class="container">
      ${renderHeader(context)}
      ${context.body || ''}
    </div>
  </body>
</html>
`;

module.exports = render;
