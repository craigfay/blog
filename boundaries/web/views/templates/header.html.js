/**
 * Dependencies
 */

/**
 * Render an html string based on a context object
 * @param {Object} context Values to be inserted into the html dynamically
 * @return {String}
 */
const render = context => `
  <div class="header">
    <div class="site-info">
      <a class="site-title" href="./">dotstar</a>
      <div class="site-subtitle">a blog by craig fay
        <span class="site-area">${`| <a href="/${context.areaLink}">${context.area || ''}</a>`}</span>
      </div>
      
    </div>
  </div>
  <div class="header-spacer"></div>
`;

module.exports = render;
