/**
 * Dependencies
 */
const blocks = require('./blocks');

/**
 * Render an html string based on a context object
 * @param {Object} context Values to be inserted into the html dynamically
 * @return {String}
 */
const render = context => `
  <div class="column-no-top">
    <div class="article">
    
      <div class="article-container">
      <div class="article-content">
        <h1 class="page-title">${context.bookTitle}</h1>
        <div class="page-subtitle">${context.bookAuthor}</div>
        <hr />
        ${context.blocks.map(block => blocks.render(block)).join('')}
      </div>
      </div>
      
    </div>
  </div>
`;

module.exports = render;