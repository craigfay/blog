/**
 * Functions that handle requests to various URLs
 */

/**
 * Dependencies
 */
const renderBase = require('./views/templates/base.html');
const renderPost = require('./views/templates/post.html');

/**
 * Export Wrapper
 */
const handlers = {};

/**
 * Handler for 'posts'
 */
handlers.blog = function blog(data, callback) {
  try {
    const { post } = data.queryStringObject;
    const context = require(`../../data/posts/${post}`);
    const html = renderBase({
      body: renderPost(context)
    });
    return callback(200, html);
  } catch (e) {
    return callback(404, 'Page not found');
  }
}

module.exports = handlers;
