/**
 * Define acceptable URLs, and how they are handled
 * 
 */

/**
 * Dependencies
 */
const handlers = require('./handlers');

/**
 * URLs and their handlers
 */
const urls = {
  'blog': handlers.blog,
};

module.exports = urls;
