/**
 * Dependencies
 */
const config = require('../../config');

module.exports = require(`./${config.persistence_module}`);
