/**
 * A persistence module that can perform crud operations based on entity data
 */

/**
 * Dependencies
 */
const fsp = require('fs').promises;
const { join } = require('path');
const { rng } = require('crypto');
const { Reply } = require('@warpstone/alpha').application;

/**
 * Export wrapper
 */
const flatJSON = {};

/**
 * @todo Consider how persistence modules will handle migrations
 */
flatJSON.ERROR_NAME = 'PERSISTENCE';

/**
 * Create an RFC4122 v4 compliant uuid
 * https://gist.github.com/jed/982883
 * https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
 */
flatJSON.uuidv4 = function uuidv4() {
  /* eslint-disable */
  return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
    (c ^ rng(1)[0] & 15 >> c / 4).toString(16)
  );
  /* eslint-enable */
};

/**
 * Persist a single object
 * @param {String} constructor The class of object to be persisted
 * @param {Object} entity The object to be persisted
 * @return {Reply}
 */
flatJSON.createOne = async function createOne(path, entity) {
  const _id = this.uuidv4(); // eslint-disable-line

  // File Data
  const filename = `${_id}.json`;
  const filepath = join(path, filename);
  const data = Object.assign({ _id }, entity);

  try {
    await fsp.writeFile(filepath, JSON.stringify(data));
    return new Reply(data);
  } catch (e) {
    if (e.code === 'ENOENT') {
      // The path to the entity does not exists
      const error = new Error(`${constructor} Entity does not exist`);
      return new Reply({}, error, this.ERROR_NAME);
    }
    // Some other write error occurred
    return new Reply({ failed: entity }, e, this.ERROR_NAME);
  }
};

/**
 * Persist multiple objects
 * @param {String} constructor The class of object to be persisted
 * @param {Array} entities The objects to be persisted
 * @return {Array} Reply objects
 */
flatJSON.createMany = async function createMany(path, entities) {
  return Promise.all(entities.map(async entity => this.createOne(path, entity)));
};

/**
 * Get persisted objects that match a given description
 * @param {String} constructor The class of objects to be searched
 * @param {Object} descriptors Key/Value pairs that the objects need to have
 * @returns {Array} Reply objects
 */
flatJSON.readMany = async function readMany(path, descriptors = {}) {
  try {
    const filenames = await fsp.readdir(path);
    const filepaths = filenames.map(f => join(path, f));

    return await this.findMatchingDocuments(filepaths, descriptors);
  } catch (e) {
    return [new Reply({}, e, this.ERROR_NAME)];
  }
};

/**
 * Get the first persisted object that match a given description
 * @param {String} constructor The class of objects to be searched
 * @param {Object} descriptors Key/Value pairs that the objects need to have
 * @returns {Reply}
 */
flatJSON.readOne = async function readOne(path, descriptors = {}) {
  try {
    const filenames = await fsp.readdir(path);
    const filepaths = filenames.map(f => join(path, f));

    const { data, error } = await this.findFirstMatchingDocument(filepaths, descriptors);

    if (error) {
      return new Reply({}, error);
    }

    return new Reply(data.entity);
  } catch (e) {
    return new Reply({}, e, this.ERROR_NAME);
  }
};

/**
 * Search JSON files for objects that match a given description
 * @param {String} dir The directory containing the documents to be searched
 * @param {Array} files Files to be searched
 * @param {Object} descriptors Key/Value pairs that the objects need to have
 * @param {Boolean} returnpath True if the filepaths of the matches should be included
 * @returns {Array} Reply Objects
 * @todo Include an options parameter to allow replies with errors to be included
 */
flatJSON.findMatchingDocuments = async function
findMatchingDocuments(filepaths, descriptors, returnpath = false) {
  const entities = await Promise.all(filepaths.map(async (file) => {
    try {
      const json = await fsp.readFile(file, 'utf8');
      const entity = JSON.parse(json);

      // Don't check descriptors if they weren't provided
      if (descriptors === {} || this.matchesDescription(entity, descriptors)) {
        // Success
        return returnpath ? new Reply({ filepath: file, entity }) : new Reply(entity);
      }
      // Values marked false correspond to filepaths that didn't produce a match
      return false;
    } catch (e) {
      // Failure, probably read related
      return new Reply({ filepath: file }, e, this.ERROR_NAME);
    }
  }));
  return entities.filter(e => e && (!e.error));
};

/**
 * Search JSON files for the first object that matches a given description
 * @param {String} dir The directory containing the documents to be searched
 * @param {Array} files Files to be searched
 * @param {Object} descriptors Key/Value pairs that the object need to have
 * @returns {Reply}
 * @todo Implement a promise race that passes back the first resolved promise
 */
flatJSON.findFirstMatchingDocument = async function
findFirstMatchingDocument(filepaths, descriptors) {
  // Loop over filepaths
  for (let i = 0; i < filepaths.length; i += 1) {
    try {
      const json = await fsp.readFile(filepaths[i], 'utf8'); // eslint-disable-line no-await-in-loop
      const entity = JSON.parse(json);

      // Don't check descriptors if they weren't provided
      if (descriptors === {} || this.matchesDescription(entity, descriptors)) {
        // Success
        return new Reply({ filepath: filepaths[i], entity });
      }
    } catch (e) {
      // Failure, probably read related
      return new Reply({ filepath: filepaths[i] }, e);
    }
  }
  // Failure, no results
  return new Reply({}, new Error('No results were found matching the given description'), 'NOT FOUND');
};

/**
 * Determine if an object has a specific set of key/value pairs
 * @param {Object} obj To be searched
 * @param {Object} descriptors To be searched for
 * @returns {Boolean}
 */
flatJSON.matchesDescription = function matchesDescription(obj, descriptors) {
  const keys = Object.keys(descriptors);

  for (let i = 0; i < keys.length; i += 1) {
    // Object does not have the property
    if (!Object.prototype.hasOwnProperty.call(obj, keys[i])) {
      return false;
    }
    // Object's property value is incorrect
    if (obj[keys[i]] !== descriptors[keys[i]]) {
      return false;
    }
  }
  return true;
};

/**
 * Overwrite persisted objects that match a given description with new properties
 * @param {String} constructor The class of objects to be searched
 * @param {Object} descriptors Key/Value pairs that the target objects need to have
 * @param {Object} amendments Key/Value pairs that the targets will have post-operation
 * @returns {Array} Reply objects
 * @todo differentiate between update and updateMany
 */
flatJSON.updateMany = async function updateMany(path, descriptors, amendments) {
  try {
    const readAttempts = await this.readMany(path, descriptors);
    return Promise.all(readAttempts.map(async (r) => {
      const amended = this.amend(r.data, amendments);
      return this.overwrite(path, amended);
    }));
  } catch (e) {
    return new Reply({}, e, this.ERROR_NAME);
  }
};

/**
 * Overwrite persisted objects that match a given description with new properties
 * @param {String} constructor The class of objects to be searched
 * @param {Object} descriptors Key/Value pairs that the target objects need to have
 * @param {Object} amendments Key/Value pairs that the targets will have post-operation
 * @returns {Array} Reply objects
 * @todo differentiate between update and updateMany
 */
flatJSON.updateOne = async function updateOne(path, descriptors, amendments) {
  try {
    const readAttempt = await this.readOne(path, descriptors);
    const amended = this.amend(readAttempt.data, amendments);
    return await this.overwrite(path, amended);
  } catch (e) {
    return new Reply({}, e, this.ERROR_NAME);
  }
};

/**
 * Overwrite a file whose id is known
 * @param {String} constructor The class of object to be overwritten
 * @param {Object} data The new data that will replace the old
 * @returns {Reply}
 */
flatJSON.overwrite = async function overwrite(path, data) {
  const filepath = join(path, `${data._id}.json`); // eslint-disable-line no-underscore-dangle

  try {
    await fsp.writeFile(filepath, JSON.stringify(data));
    return new Reply(data);
  } catch (e) {
    return new Reply({}, e, this.ERROR_NAME);
  }
};

/**
 * Return an object with overwritten or newly added properties
 * @param {Object} obj
 * @param {Object} amendments
 * @returns {Object}
 */
flatJSON.amend = function amend(obj, amendments) {
  return Object.assign(obj, amendments);
};

/**
 * Delete a persisted entity based on a description
 * @param {String} constructor The class of object to be overwritten
 * @param {Object} descriptors Key/Value pairs that the target objects need to have
 * @return {Reply}
 */
flatJSON.deleteOne = async function deleteOne(path, descriptors) {
  try {
    const filenames = await fsp.readdir(path);
    const filepaths = filenames.map(f => join(path, f));

    const { data, error } = await this.findFirstMatchingDocument(filepaths, descriptors);

    // Probably a 'NOT FOUND'
    if (error) {
      return new Reply({}, error);
    }

    // Delete the target file
    await fsp.unlink(data.filepath);

    // Re-package the data, not including the filepath
    return new Reply(data.entity);
  } catch (e) {
    return new Reply({}, e, this.ERROR_NAME);
  }
};

/**
 * Delete multiple persisted entities based on a description
 * @param {String} constructor The class of object to be overwritten
 * @param {Object} descriptors Key/Value pairs that the target objects need to have
 * @return {Array} Reply objects
 */
flatJSON.deleteMany = async function deleteMany(path, descriptors) {
  try {
    const filenames = await fsp.readdir(path);
    const filepaths = filenames.map(f => join(path, f));

    const matches = await this.findMatchingDocuments(filepaths, descriptors, true);

    return Promise.all(matches.map(async (m) => {
      try {
        await fsp.unlink(m.data.filepath);
        return new Reply(m.data.entity);
      } catch (e) {
        return new Reply({}, e, this.ERROR_NAME);
      }
    }));
  } catch (e) {
    return new Reply({}, e, this.ERROR_NAME);
  }
};

module.exports = flatJSON;
