/**
 * An adapter for the json persistence module
 */

/**
 * Dependencies
 */
const config = require('../../../config');
const { Reply } = require('@warpstone/alpha').application;
const { existsSync, mkdirSync } = require('fs');
const { join } = require('path');
const json = require('./json');

const path = constructor => join(config.persist_dir, constructor.name);

/**
 * Export Wrapper
 */
const persistence = {};

/**
 * Create the persistence directories if they don't exist yet
 * @todo this is actually a migration
 */
persistence.start = function start() {
  // Confirm the existence of the base persistence directory
  if (!existsSync(config.persist_dir)) {
    mkdirSync(config.persist_dir);
  }

  // Confirm that each entity has a persistence subdirectory
  Object.keys(config.entities).map((entity) => {
    const dir = join(config.persist_dir, entity);
    if (!existsSync(dir)) {
      mkdirSync(dir);
    }
    return true;
  });
  console.info(' > Persistence Ready');
};

persistence.createOne = async function createOne(entity) {
  const reply = await json.createOne(path(entity.constructor), entity.fields);
  delete reply.data._id;
  try {
    return (reply.error) ? reply : new Reply(new entity.constructor(reply.data)); 
  } catch (e) {
    return new Reply({}, e);
  }
};

persistence.createMany = function createMany(entity) {
  return json.createOne(path(entity.constructor), entity.fields);
};

persistence.readOne = function readOne(constructor, descriptors) {
  const reply = json.readOne(path(constructor), descriptors);
  return eval(`new ${constructor.constuctor.name}(reply.data)`);
};

persistence.readMany = function readMany(constructor, descriptors) {
  return json.readMany(path(constructor), descriptors);
};

persistence.updateOne = function updateOne(constructor, descriptors, amendments) {
  return json.updateOne(path(constructor), descriptors, amendments);
};

persistence.updateMany = function updateMany(constructor, descriptors, amendments) {
  return json.updateMany(path(constructor), descriptors, amendments);
};

persistence.deleteOne = function deleteOne(constuctor, descriptors) {
  return json.deleteOne(path(constructor), descriptors);
};

persistence.deleteMany = function deleteMany(constructor, descriptors) {
  return json.deleteMany(path(constructor), descriptors);
};

module.exports = persistence;
