/**
 * Configuration Variables for cbfay.com
 */

/**
 * Dependencies
 */
const { join } = require('path');

/**
 * Export Wrappers
 */
const config = {};
const testconfig = {};

/**
 * Development Variables
 */
config.debug = true;
config.dev = true;

/**
 * Web Variables
 */
config.httpPort = process.env.PORT || 8000;
config.staticWebContent = join(__dirname, 'boundaries', 'web', 'views', 'public');

/**
 * Persistence Variables
 */
config.persistence_module = 'json';
config.persist_dir = join(__dirname, '.data');

/**
 * Entities
 */
config.entities = {
  Animal: join(__dirname, '_Animal', 'class.js'),
  Human: join(__dirname, '_Human', 'class.js'),
}

module.exports = process.env.NODE_ENV != 'test' ? config : testconfig;
